#!/usr/bin/env csh
    
# JM 17/5/2013
# copy source code from Geant4 examples packages
# Currently only uses extended examples Hadr00, TestEM7, TestEM9. Can be extended to copy others

set G4EXAM = 'examples/extended'
  
set here = $PWD
    
set pack = `cmt -quiet show macro_value package | tr -d "G4"`
set parent=`cmt -quiet show macro_value package | tr -d "G4" | tr -d '[:digit:]'`

echo ${parent}

if( ${parent} == 'Hadr' ) then
    set G4Dir="hadronic/"${pack}    
else if( ${parent} == 'TestEm' ) then
    set G4Dir="electromagnetic/"${pack}
else
    echo 'Warning! Unkown package: '${pack}
    set G4Dir=""
endif
    
set p = 'src'
set q = 'include'

cd $here/..
 
if !( -d ${p} ) then
    mkdir -p ${p}
    $G4_UNIX_COPY ${G4SHARE}/${G4EXAM}/${G4Dir}/${p}/*.* ${p}/.
    $G4_UNIX_COPY ${G4SHARE}/${G4EXAM}/${G4Dir}/*.cc ./.
    echo ' source files have been copied from '${G4SHARE}/${G4EXAM}/${G4Dir}
else
    echo ''${p}' exists - skip copy'
endif

#copy local version of include files

if !( -d ${q}) then
    mkdir -p ${q}
    $G4_UNIX_COPY ${G4SHARE}/${G4EXAM}/${G4Dir}/include/*.* ${q}/.
    echo ' include files have been copied from '${G4SHARE}/${G4EXAM}/${G4Dir}
else
    echo ''${q}' exists - skip copy'
endif

cd $here

if( (${pack} == 'TestEm3')||(${pack} == 'TestEm5') ) then
    ./copyPatchedSource.py
echo ' executed copyPatchedSource.py '
endif

