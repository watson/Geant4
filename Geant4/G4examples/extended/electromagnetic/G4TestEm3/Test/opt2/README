G4TestEm3 is designed to model a sampling electromagnetic calorimeter.  The number of layers, layer materials, layer thicknesses in the model of the calorimeter are all customisable, see picture below. 

	|<----layer 0---------->|<----layer 1---------->|<----layer 2---------->|
	|			|			|			|
	==========================================================================
	||		|	||		|	||		|	||
	||		|	||		|	||		|	||
	||   abs 1	| abs 2	||   abs 1	| abs 2	||   abs 1	| abs 2	||
	||		|	||		|	||		|	||
	||		|	||		|	||		|	||
 beam	||		|	||		|	||		|	||
======>	||		|	||		|	||		|	||
	||		|	||		|	||		|	||
	||		|	||		|	||		|	||
	||		|	||		|	||		|	||
	||		|	||		|	||		|	||
	||    cell 1	| cell 2||    cell 3	| cell 4||    cell 5	| cell 6||
	==========================================================================
	^		^	^		^	^		^	^
	pln1		pln2	pln3		pln4	pln5		pln6	pln7


It has been tuned to model the LHCb ECal as closely as possible, with 66 layers each consisiting of a 2mm thick slice of lead and a 4mm thick slice of absorber.  Electrons at 13 different energys have been fired into the block of calorimeter with the aim of extracting an energy resolution for the model of calorimeter.  This has been done by plotting the distribution of total energy deposited per event in scintilator layers and fitting a gaussian to extract the mean and sigma.  The resolution for one specific energy is then sigma/mean, which has been plotted against 1/sqrt(E), where E is the energy of the incident electrons as specified in the simulation.  The usual parameterisation of calorimeter resolution is A/sqrt(E) added in quadrature with C, where A and C are determined by a fit of sigma/E against 1/sqrt(E).  Results for A and C are given in the text file supplied and the corresponding fits are in TGraphErrors "Resolution1" and "Resolution2".

The sampling fraction of this calorimeter has also been studied, where sampling fraciton is defined as total energy deposited in scintillator over total energy deposited in lead.  This has been done by looking at longitudinal shower profiles (supplied in the 13 canvases in the root file), which have been integrated to give the total energy deposited in absorber and lead.  This has then been plotted as a function of energy, which is shown in the TGraphErrors "sampling".  The numerical result for the sampling fraction at each energy is given in the text file supplied.

If you need to know anything else my email address is timothy.williams@cern.ch and i'm in office 313.

