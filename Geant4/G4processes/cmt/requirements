#============================================================================
# Created    : 2008-02-20
# Maintainer : Gloria CORTI
#============================================================================
package           G4processes
version           v9r2

# Structure, i.e. directories to process. 
#============================================================================
branches          cmt doc G4processes

# Dependencies 
#============================================================================
use   G4digits_hits   v*      Geant4
#nkw 26/7/2012 use   G4externalslcg  v*      Geant4
#nkw
use   Expat          v*       LCG_Interfaces
# apply_pattern install_more_includes more=G4processes

include_dirs $(G4processes_root)/G4processes 

# Library
#============================================================================
library        G4processes $(G4LibraryFlags) \
                            ../management/src/*.cc                  \
                            ../cuts/src/*.cc                        \
                            ../optical/src/*.cc                     \
                            ../decay/src/*.cc                       \
                            ../scoring/src/*.cc                     \
                            ../biasing/src/*.cc                     \
                            ../transportation/src/*.cc              \
                            ../parameterisation/src/*.cc            \
                      -s=../electromagnetic            \
                               adjoint/src/*.cc                     \
                               muons/src/*.cc                       \
                               standard/src/*.cc                    \
                               utils/src/*.cc                       \
                               xrays/src/*.cc                       \
                               lowenergy/src/*.cc                   \
                               highenergy/src/*.cc                  \
                               pii/src/*.cc                         \
                               polarisation/src/*.cc                       

# The DNA library cannot be used with this version of CLHEP
# Not useful for LHCb anyway, but can be included later 
#	              -s=../electromagnetic/dna/           \
#			       management/src/*.cc                  \
#                     	       untils/src/*.cc                      \
#			       molecules/src/*.cc                   \
#                               models/src/*.cc                      \
#                               processes/src/*.cc                   
                                
# the library had to be split in 2 parts: there is too many symbols
# exported for the windows linker otherwise (>32768), Hubert DeGaudenzi

library        G4processeshad $(G4LibraryFlags) \
                            ../hadronic/cross_sections/src/*.cc     \
                            ../hadronic/management/src/*.cc         \
                            ../hadronic/util/src/*.cc               \
                            ../hadronic/processes/src/*.cc          \
                            ../hadronic/stopping/src/*.cc           \
                     -s=../hadronic/models            \
                               abrasion/src/*.cc                    \
                               binary_cascade/src/*.cc              \
                               coherent_elastic/src/*.cc            \
                               em_dissociation/src/*.cc             \
                               high_energy/src/*.cc                 \
                               im_r_matrix/src/*.cc                 \
                               isotope_production/src/*.cc          \
      			       lend/src/*.cc                        \
                               lepto_nuclear/src/*.cc               \
                               lll_fission/src/*.cc                 \
                               low_energy/src/*.cc                  \
                               management/src/*.cc                  \
                               neutron_hp/src/*.cc                  \
                               qmd/src/*.cc                         \
	     		       quasi_elastic/src/*.cc		    \	
                               radioactive_decay/src/*.cc           \
                               rpg/src/*.cc                         \
                               theo_high_energy/src/*.cc            \
                               util/src/*.cc                        \
                      -s=../hadronic/models/cascade \
                               cascade/src/*.cc                     \
                               evaporation/src/*.cc                 \
                               utils/src/*.cc                       \
                      -s=../hadronic/models/chiral_inv_phase_space \
                               body/src/*.cc                        \
                               cross_sections/src/*.cc              \
                               fragmentation/src/*.cc               \
                               interface/src/*.cc                   \
                               processes/src/*.cc                   \
                      -s=../hadronic/models/de_excitation  \
                               ablation/src/*.cc                    \
                               evaporation/src/*.cc                 \
                               fermi_breakup/src/*.cc               \
                               fission/src/*.cc                     \
                               gem_evaporation/src/*.cc             \
                               handler/src/*.cc                     \
                               management/src/*.cc                  \
                               multifragmentation/src/*.cc          \
                               photon_evaporation/src/*.cc          \
                               util/src/*.cc                        \
                       -s=../hadronic/models/inclxx/  \
                               incl_physics/src/*.cc                \
                               interface/src/*.cc                   \
                               utils/src/*.cc                       \
                       -s=../hadronic/models/parton_string  \
                               diffraction/src/*.cc                 \
                               hadronization/src/*.cc               \
                               management/src/*.cc                  \
                               qgsm/src/*.cc                        \
                      -s=../hadronic/models/pre_equilibrium         \
                               exciton_model/src/*.cc

# Patterns
#============================================================================
apply_pattern  G4AllCMTpatterns

# apply complement patterns for the second library
apply_pattern library_Softlinks library=G4processeshad
apply_pattern library_Llinkopts library=G4processeshad
apply_pattern library_Lshlibflags library=G4processeshad
apply_pattern linker_library    library=G4processeshad

macro_append G4processeshad_use_linkopts " -lG4processes" \
	                              WIN32 " G4processes.lib "
set_remove 
macro_append G4processeshad_dependencies G4processes

#============================================================================



private

apply_pattern G4_copy_source

# Run ad hoc copying of patched sources from G4config/copy_source.*sh

#action copy_patched_source "./copyPatchedSource.py" 
#Defined copying additional files

# Run the action copy_patched_includes when calling make
#macro_append constituents  " copy_patched_source"

# Prevent actions to be run twice when calling 'make all_groups'
#macro_remove cmt_actions_constituents "copy_patched_source"
end_private
